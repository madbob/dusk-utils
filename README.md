# Dusk Utils

A few utilities for unit testing with [Laravel Dusk](https://laravel.com/docs/dusk).

- `typeAtXPath($expression, $value)`: similar to the native `type` but uses a XPath expression as target
- `assertInputValueAtXPath($expression, $value)`: similar to the native `assertInputValue` but uses a XPath expression as target
- `assertSeeAtXPath($expression, $value)`: similar to the native `assertSee` but uses a XPath expression as target
- `scrollView($selector)`: similar to the native `scrollIntoView` but scrolls the element at the end of the viewport, not the beginning
- `scrollTop()`: scrolls the viewport to the top
- `scrollBottom()`: scrolls the viewport to the bottom
- `mainScreenshot($filename)`: similar to the native `screenshot` but scrolls the viewport to the top before taking the shot

## Installation

```
composer require madbob/dusk-utils
```

Then in the `boot()` function of your preferred Service Provided (AppServiceProvider?) add the line

```
\MadBob\DuskUtils\Utils::init();
```

to init the extra macros.
