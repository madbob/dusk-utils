<?php

namespace MadBob\DuskUtils;

class Utils
{
    public static function init()
    {
        /*
            Hopefully this will be included directly into Laravel Dusk...
            https://github.com/laravel/dusk/pull/895
        */
        \Laravel\Dusk\Browser::macro('typeAtXPath', function ($expression, $value) {
            $this->driver->findElement(\Facebook\WebDriver\WebDriverBy::xpath($expression))->clear()->sendKeys($value);
            return $this;
        });

        \Laravel\Dusk\Browser::macro('assertInputValueAtXPath', function ($expression, $value) {
            $input_value = $this->driver->findElement(\Facebook\WebDriver\WebDriverBy::xpath($expression))->getAttribute('value');

            \PHPUnit\Framework\Assert::assertEquals(
                $value,
                $input_value,
                "Expected value [{$value}] for the [{$expression}] input does not equal the actual value [${input_value}]."
            );

            return $this;
        });

        \Laravel\Dusk\Browser::macro('assertSeeAtXPath', function ($expression, $value) {
            $input_value = $this->driver->findElement(\Facebook\WebDriver\WebDriverBy::xpath($expression))->getText();

            \PHPUnit\Framework\Assert::assertEquals(
                $value,
                $input_value,
                "Expected value [{$value}] for the [{$expression}] element does not equal the actual value [${input_value}]."
            );

            return $this;
        });

        \Laravel\Dusk\Browser::macro('scrollView', function ($selector) {
            $selector = addslashes($this->resolver->format($selector));
            $this->driver->executeScript("document.querySelector(\"$selector\").scrollIntoView({block: 'end'});");
            $this->pause(500);
            return $this;
        });

        \Laravel\Dusk\Browser::macro('scrollTop', function () {
            $this->script('document.documentElement.scrollTop = 0');
            return $this;
        });

        \Laravel\Dusk\Browser::macro('scrollBottom', function () {
            $this->script('window.scrollTo(0, document.body.scrollHeight)');
            return $this;
        });

        \Laravel\Dusk\Browser::macro('mainScreenshot', function ($filename) {
            $this->scrollTop()->pause(1000)->screenshot($filename);
            return $this;
        });
    }
}
